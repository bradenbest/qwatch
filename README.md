# qwatch -- Quick Watch

## Installation

For a first time install:

    $ git clone https://gitlab.com/bradenbest/qwatch.git
    $ cd qwatch/
    $ ./install -s
    $ sudo ./install

To elaborate on the installer:

    Usage: [sudo] ./install [option]
    Options:
        -s  Setup. Run this for first install only. Do not run with sudo
        -u  Uninstall. Running this will purge everything
    Given no options, the installer will install/update.

To make yourself a good config file, open qwatch in a text editor to around `line 30` or so, and copy the "USER CONFIG" section into `~/.qwatch/qwatch.conf.`

You can also take a look at the `example_scripts/` directory for some snippets you can add to `~/.qwatch/functions/`

## Updating

### Automatic

By default qwatch will automatically check for updates upon starting, though you can disable that by adding the following to your `qwatch.conf`:

    auto_update=0

or by running

    $ qwatch --no-auto-update

### Semi-automatic (qwatch)

To update, all you have to do is run

    $ qwatch --update

or run `update` from within qwatch.

### Manual (git)

It happens. Sometimes I'll change something and it will break the updater. Although this is rare, if it does happen, you can follow similar steps to the installation process

    $ git clone https://gitlab.com/bradenbest/qwatch.git
    $ cd qwatch/
    $ sudo ./install

This will update using the installer as a last resort.

For more information, see `help:Auto Update`

In layman's terms, that means:

1. run `qwatch --help "Auto Update"`
2. run `help Auto Update` from within qwatch.
3. `help:Auto Update` also works

## Usage

    $ qwatch [directory] [options]

For a list of options, see `help:Arguments`

For the full manual, run

    $ qwatch --help

Besides that, qwatch is a CLI app, so for a list of commands, type `h` from within qwatch, or see `help:Commands` and `help:Custom Commands` for detailed documentation.

## What is qwatch for?

I designed it for managing/burning through videos, but this can be used with multiple kinds of media, as the viewer itself can be changed to anything. For example...

    vlc (default) - for videos, music
    aplay - for listening to raw audio
    mplayer - for music and videos
    firefox - to view an swf
    animate - to view a gif (requires imagemagick)
    feh - pictures
    fbi - pictures (when in TTY)
    cat - text files
    vi - editing text files

Default viewer is `vlc --fullscreen --play-and-exit`

For information regarding changing defaults, see `help:User Configuration` or use the `vwr` command
