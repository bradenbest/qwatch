In functions/, two example scripts you can put in your ~/.qwatch/functions directory

1. _sort - This is a slightly modified version of the function used in the example in the help docs

2. _autoplay_mv - this is a replacement function for autoplay's default action. With this installed, autoplay will move the selected file to `../watched/` instead of deleting it.

In config/, two example scripts you can put anywhere and use with `qwatch -c`.

1. qwatch.base - I use this as a base for a lot of my scripts

2. qwatch.show - an example script that uses `qwatch.base` for its filtering facilities

To use, `qwatch -c qwatch.show`, then type 'h' for help. Notice the `user-defined` section at the bottom.
